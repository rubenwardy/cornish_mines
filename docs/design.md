# Story

* Start with a basic pit and a few miners.
* Drag out paths for the miners to mine.
* Place ladders, lamps, etc.
* Unlock and build engine houses.
* Manage risks:
	* Flooding whilst underwater
	* Explosions (some tiles will release explosive gas)
