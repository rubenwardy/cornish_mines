# Cornish Mining

[Play Online](https://rubenwardy.com/ld48_cornish_mines/) | [LDJAM 48](https://ldjam.com/events/ludum-dare/48/tin-mining)

License: GPLv3 or later.

**HERE BE DRAGONS:** This was created as an
[LDJAM 48 entry](https://ldjam.com/events/ludum-dare/48/tin-mining), and so was
made under extreme time constraints. Don't expect too much code quality.
