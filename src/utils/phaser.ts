export type Vec2 = Phaser.Math.Vector2;

export function V(x?: number | Phaser.Types.Math.Vector2Like, y?: number) {
	return new Phaser.Math.Vector2(x, y);
}

export function FloorV(pos: Vec2): Vec2 {
	return V(Math.floor(pos.x), Math.floor(pos.y));
}

export function RoundV(pos: Vec2): Vec2 {
	return V(Math.round(pos.x), Math.round(pos.y));
}

export function HashV(pos: Vec2) {
	return pos.x + pos.y * 10000;
}

export function MinMaxV(one: Vec2, two: Vec2): [Vec2, Vec2] {
	return [
		V(Math.min(one.x, two.x), Math.min(one.y, two.y)),
		V(Math.max(one.x, two.x), Math.max(one.y, two.y)),
	];
}
