import { createFalse } from "typescript";
import { HashV, V, Vec2 } from "utils/phaser";
import World from "world/World";

const xNeighbours = [ V(-1, 0), V(1, 0), ];
const yNeighbours = [ V(0, -1), V(0, 1), ];

class Node {
	constructor(public parent: (Node | null), readonly position: Vec2,
		public distanceGone: number, readonly estimatedToGo: number) {}

	get heuristic() {
		return this.distanceGone + this.estimatedToGo;
	}
}

interface Options {
	acceptNeighbour: boolean;
}

export default class Pathfinder {
	constructor(private world: World) {}

	findPath(start: Vec2, target: Vec2, options?: Options): (Vec2[] | null) {
		if (!options) {
			options = {
				acceptNeighbour: false,
			};
		}

		const timeoutLimit = Math.max(30, target.distance(start) * 10);

		console.log(`Pathfinding from (${start.x.toFixed(1)}, ${start.y.toFixed(1)}) to (${target.x.toFixed(1)}, ${target.y.toFixed(1)}). Limit=${timeoutLimit}`);

		const open: Node[] = [];
		const visited = new Set<number>();
		const openLookup = new Map<number, Node>();

		function isDone(pos: Vec2) {
			if (pos.equals(target)) {
				return true;
			}

			if (options.acceptNeighbour) {
				const delta = target.clone().subtract(pos);
				return (Math.abs(delta.x) == 1 && delta.y == 0) ||
						(delta.x == 0 && Math.abs(delta.y) == 1);
			}

			return false;
		}

		function addNode(parent: (Node | null), position: Vec2, weight: number) {
			const hash = HashV(position);
			if (visited.has(hash)) {
				return;
			}

			const distanceGone = parent ? parent.distanceGone + weight : 0;
			if (distanceGone > timeoutLimit) {
				return;
			}

			const estimatedToGo = position.distanceSq(target);
			let node = openLookup.get(hash);
			if (!node) {
				node = new Node(parent, position.clone(), distanceGone, estimatedToGo);
				open.push(node);
				openLookup.set(hash, node);
			} else if (node.distanceGone > distanceGone) {
				node.parent = parent;
				node.distanceGone = distanceGone;
			}
		}

		addNode(null, start, 0);

		let checks = 0;
		while (open.length > 0) {
			checks++;
			if (checks > 4000) {
				console.error("Check timeout reached");
				return;
			}

			const [bestNode, bestIdx] = open.reduce<[Node, number]>((best, current, idx) => {
				if (!best[0] || current.heuristic < best[0].heuristic) {
					return [current, idx];
				}

				return best;
			}, [ null, 0]);

			open.splice(bestIdx, 1);

			if (isDone(bestNode.position)) {
				const path: Vec2[] = [];
				let node = bestNode;
				while (node) {
					path.push(node.position);
					node = node.parent;
				}

				return path.reverse();
			}

			xNeighbours.forEach((offset) => {
				const nextPos = bestNode.position.clone().add(offset);
				const nextPosUp = V(nextPos.x, nextPos.y - 1);
				const nextPosDown = V(nextPos.x, nextPos.y + 1);
				const up = bestNode.position.clone().add(V(0, -1));

				// Walk horizontal
				if (this.world.canStandAt(nextPos)) {
					addNode(bestNode, nextPos, 1);
				}

				// Drop down step
				if (!this.world.collidesAt(nextPos) && this.world.canStandAt(nextPosDown)) {
					addNode(bestNode, nextPosDown, 1.2);
				}

				// Jump
				if (!this.world.collidesAt(up) && this.world.canStandAt(nextPosUp)) {
					addNode(bestNode, nextPosUp, 1.2);
				}
			});

			if (this.world.climbableAt(bestNode.position)) {
				yNeighbours.forEach((offset) => {
					const nextPos = bestNode.position.clone().add(offset);
					if (!this.world.collidesAt(nextPos)) {
						addNode(bestNode, nextPos, 1.8);
					}
				})
			}
		}

		console.warn("No path found");
		return null;
	}
}



let pathfinder: Pathfinder;

export function setPathfinder(pf: Pathfinder) {
	pathfinder = pf;
}

export function getPathfinder(): Pathfinder {
	return pathfinder;
}
