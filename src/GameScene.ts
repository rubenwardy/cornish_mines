import World from "world/World";
import "phaser";
import WorldObject from "WorldObject";
import generateMap from "world/Mapgen";
import { CameraControl } from "CameraControl";
import { Tool, ToolSelectType, ToolSpec, ToolType, ToolUser } from "tools";
import BottomBar from "components/BottomBar";
import ToolBox from "components/ToolBox";
import DefinitionManager, { defineDefaults, Layer, TileIds } from "world/DefinitionManager";
import { MineTool } from "tools/MineTool";
import { V, Vec2 } from "utils/phaser";
import Pathfinder, { setPathfinder } from "Pathfinder";
import { CancelTool } from "tools/CancelTool";
import { BuildTool } from "tools/BuildTool";
import WorkerEntity from "world/WorkerEntity";
import ItemEntity from "world/ItemEntity";
import { EntityType } from "world/Entity";

declare global {
	interface Window {
		game: GameScene;
	}
}

export class GameScene extends Phaser.Scene implements ToolUser {
	definitionManager: DefinitionManager;
	world: World;
	money = 0;
	isDebug = false;

	private worldObject: WorldObject;
	private sky: Phaser.GameObjects.Graphics;
	private controls: CameraControl;
	private hudElement: HTMLDivElement;
	private debugInfoElement: HTMLSpanElement;
	private _tool?: Tool = null;
	private bottomBar: BottomBar;
	private nextHudUpdate = 0;
	private nextStockpileSell = 0;

	get tool(): (Tool | null) {
		return this._tool;
	}

	set tool(v: (Tool | null)) {
		this._tool?.deactivate();
		this._tool = v;

		if (this._tool) {
			this._tool.activate();
		}
	}

	constructor() {
		super({ key: "GameScene" });

		window.game = this;
	}

	create() {
		this.hudElement = document.getElementById("hud") as HTMLDivElement;
		this.debugInfoElement = document.createElement("span");
		this.hudElement.appendChild(this.debugInfoElement);

		this.bottomBar = new BottomBar(this.hudElement);

		this.bottomBar.addTab({
			label: "Build",
			func: ToolBox,
			props: {
				tools: this.getToolSpecs(),
				onClicked: (spec) => this.setToolFromSpec(spec),
			},
		});

		this.bottomBar.addStats();
		this.bottomBar.updateStats({
			Money: Math.round(this.money),
		});

		this.definitionManager = new DefinitionManager();
		defineDefaults(this.definitionManager);

		this.world = new World(this.add, this.definitionManager, 128, 128);
		generateMap(this.world);
		setPathfinder(new Pathfinder(this.world));

		const minePos = this.world.buildings[0].position.clone().add(V(1.5, 12)).scale(8);
		this.cameras.main.centerOn(minePos.x, minePos.y);

		this.setSky();
		this.worldObject = new WorldObject(this, this.world, this.definitionManager);

		var cursors = this.input.keyboard.createCursorKeys();
		var controlConfig = {
			camera: this.cameras.main,
			left: cursors.left,
			right: cursors.right,
			up: cursors.up,
			down: cursors.down,
			zoomIn: this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.Z),
			zoomOut: this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.Q),
			acceleration: 0.3,
			drag: 0.01,
			maxSpeed: 1
		};

		this.controls = new CameraControl(this.input, controlConfig);

		for (let i = 0; i < 10; i++) {
			this.recruitWorker();
		}

		this.input.keyboard.on("keyup-ESC", () => {
			this.tool = null;
		});
	}

	recruitWorker() {
		const x = Math.floor(Math.random() * 20) + 45;
		const entity = new WorkerEntity(this.world, "worker",
				V(x + 0.5, this.world.getHeightAt(Layer.TERRAIN, Math.floor(x))));
		this.world.addEntity(entity);
	}

	getToolSpecs(): ToolSpec[] {
		const ret: ToolSpec[] = [
			{
				type: ToolType.Cancel,
				label: "Cancel",
				icon: "cancel",
				selectType: ToolSelectType.Filled,
			},

			{
				type: ToolType.Mine,
				label: "Mine",
				icon: "mine",
				selectType: ToolSelectType.Filled,
			},

			{
				type: ToolType.Build,
				label: "Ladder",
				icon: "ladder",
				selectType: ToolSelectType.Vertical,
				info: {
					layer: Layer.TILE,
					tile: TileIds.LADDER,
				},
			},
		];

		return ret;
	}

	setToolFromSpec(spec: ToolSpec) {
		switch (spec.type) {
		case ToolType.Cancel:
			this.tool = new CancelTool(this, this.input, spec);
			break;

		case ToolType.Mine:
			this.tool = new MineTool(this, this.input, spec);
			break;

		case ToolType.Build:
			this.tool = new BuildTool(this, this.input, spec);
			break;
		}
	}

	setSky() {
		const top = 0x124e89;
		const bottom = 0x0099db;

		this.sky = this.add.graphics();
    	this.sky.fillGradientStyle(top, top, bottom, bottom);
		this.sky.fillRect(0, -10 * 8, this.world.width * 8, 40 * 8);
	}

	update(time: number, dtime: number) {
		if (this.tool) {
			this.tool.update(dtime);
		}

		const actualDtime = Math.min(this.game.loop.rawDelta, 80) / 1000;

		this.world.update(actualDtime);
		this.worldObject.update();
		this.controls.update(dtime);

		if (time > this.nextHudUpdate) {
			this.nextHudUpdate += 200;
			this.updateHud();
		}

		if (time > this.nextStockpileSell) {
			this.nextStockpileSell += 1000 * (2 * Math.random() + 1);
			for (let itemEntity of this.world.getEntitiesOfType<ItemEntity>(EntityType.Item)) {
				if (!itemEntity.parent && itemEntity.isInStockpile()) {
					this.world.removeEntity(itemEntity);

					const item = itemEntity.item;
					if (item == "stone") {
						this.money += 1;
					} else if (item == "tin_ore") {
						this.money += 1000;
					} else {
						console.error("Unknown item: " + item);
					}

					this.bottomBar.updateStats({
						Money: Math.round(this.money),
					});
				}
			}
		}
	}

	updateHud() {
		if (this.isDebug) {
			const mp = this.getMousePosition();

			const workerInfo =
				this.world.getEntitiesOfType<WorkerEntity>(EntityType.Worker)
						.filter(worker => worker.position.distanceSq(mp) < 2)
						.map(worker => worker.dump());

			this.debugInfoElement.innerText =
				(`FPS: ${this.game.loop.actualFps.toFixed(0)} (${(1000 / this.game.loop.rawDelta).toFixed(0)})
				Cursor: ${mp.x}, ${mp.y}
				${workerInfo.join("\n")}`);
		}
	}

	getMousePosition(): Vec2 {
		return this.worldObject.getMousePosition(this.input.activePointer);
	}
}
