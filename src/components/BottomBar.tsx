import { PreactWrapper } from "components";
import preact = require("preact");

interface Tab {
	label: string;
	func?: (props: any) => any;
	props?: any;
}

interface BottomBarProps {
	tabs: Tab[];
	stats: { [key: string]: number };
}

function BottomBarFC(props: BottomBarProps) {
	const tabs = props.tabs.map(tab => {
		if (tab.func) {
			const Func = tab.func;
			return (
				<li key={tab.label}>
					<span>{tab.label}</span>
					<div className="content">
						<Func {...tab.props} />
					</div>
				</li>);
		} else {
			const alias = { Money: "£" };
			const stats = Object.entries(props.stats)
				.map(([key, value]) => `${alias[key] ?? key} ${value.toFixed(0)}`)
				.join("  ");

			return (
				<li key={tab.label}>
					<span>{stats}</span>
				</li>);
		}
	});

	return (
		<ul class="bottombar">{tabs}</ul>);
}


export default class BottomBar {
	private props: BottomBarProps = { tabs: [], stats: {} }
	private wrapper: PreactWrapper<BottomBarProps>;

	constructor(parent: HTMLElement) {
		this.wrapper =
			new PreactWrapper<BottomBarProps>(parent, BottomBarFC, { ...this.props });
	}

	addTab(tab: Tab) {
		this.props.tabs.push(tab);
		this.build();
	}

	addStats() {
		this.props.tabs.push({ label: "" });
		this.build();
	}

	updateStats(stats: { [key: string]: number }) {
		this.props.stats = stats;
		this.build();
	}

	private build() {
		this.wrapper.update({ ...this.props });
	}
}
