import preact = require("preact");

export class PreactWrapper<T> {
	private root: HTMLElement;

	constructor(private parent: HTMLElement, private func: (props: T) => any, props?: T) {
		this.root = document.createElement("div");
		this.parent.appendChild(this.root);

		if (props) {
			this.update(props);
		}
	}

	update(props: T) {
		const Element = this.func;
		preact.render(<Element {...props} />, this.parent, this.root);
	}
}
