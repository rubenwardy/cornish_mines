import { ToolSpec } from "tools";

interface ToolBoxProps {
	tools: ToolSpec[];
	onClicked: (tool: ToolSpec) => void;
}

export default function ToolBox(props: ToolBoxProps) {
	const tools = props.tools.map(tool => (
		<li key={tool.label} onClick={() => props.onClicked(tool)}>
			<i class={`icon icon-${tool.icon}`}></i>
			<span>{tool.label}</span>
		</li>));

	return (
		<ul className="toolbox">{tools}</ul>);
}
