import "phaser";
import { V, Vec2 } from "utils/phaser";

export class CameraControl extends Phaser.Cameras.Controls.SmoothedKeyControl {
	dragOrigin?: Vec2

	constructor(input: Phaser.Input.InputPlugin,
			config: Phaser.Types.Cameras.Controls.SmoothedKeyControlConfig) {
		super(config);

		this.camera.zoom = 4;

		input.on("pointermove", (pointer: Phaser.Input.Pointer) => {
			if (this.dragOrigin) {
				let camera = this.camera;
				let delta = V(pointer.positionToCamera(camera))
					.subtract(this.dragOrigin);
				camera.scrollX -= delta.x;
				camera.scrollY -= delta.y;
			}
		});

		input.on("pointerup", (pointer: Phaser.Input.Pointer) => {
			if (pointer.button == 1 || pointer.button == 2) {
				this.dragOrigin = null;
			}
		});

		input.on("pointerdown", (pointer: Phaser.Input.Pointer) => {
			if (pointer.button == 1 || pointer.button == 2) {
				this.dragOrigin =
					V(pointer.positionToCamera(this.camera));
			}
		});

		input.on("wheel", (_pointer: Phaser.Input.Pointer, _1, _2, deltaY: number) => {
			let zoom = this.camera.zoom - deltaY * 0.004;
			zoom = Math.min(8, Math.max(2, zoom));
			this.camera.setZoom(zoom);
		});
	}
}
