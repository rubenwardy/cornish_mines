import "phaser";

export class LoadingScene extends Phaser.Scene {
	constructor() {
		super({ key: "LoadingScene" });
	}

	preload() {
		this.load.image("icons", "assets/icons.png");
		this.load.image("tileset", "assets/tileset.png");
		this.load.spritesheet("items", "assets/items.png", { frameWidth: 8, frameHeight: 8 });
		this.load.image("blueprint", "assets/blueprint.png");
		this.load.spritesheet("worker", "assets/worker.png", { frameWidth: 3, frameHeight: 6 });
	}

	create() {
		this.add.text(20, 20, "Loading...").setOrigin(0,0);

		this.load.on("complete", () => {
			this.game.scene.start("GameScene");
			this.game.scene.stop("LoadingScene");
			this.game.scene.swapPosition("GameScene", "LoadingScene");
		});

		this.load.start();
	}
}
