import { posix } from "node:path";
import { Tool, ToolSelectType, ToolUser } from "tools";
import { MinMaxV, V, Vec2 } from "utils/phaser";


export default class SelectHelper implements Tool {
	private selectionStart: Vec2;
	private rect: Phaser.GameObjects.Rectangle;

	constructor(private input: Phaser.Input.InputPlugin,
			private selectType: ToolSelectType, private game: ToolUser,
			private listener: (tiles: Vec2[]) => void) {}

	private onDown = (pointer: Phaser.Input.Pointer) => {
		if (pointer.button == 0) {
			this.start();
		}
	}

	private onUp = (pointer: Phaser.Input.Pointer) => {
		if (pointer.button == 0) {
			const results = this.getSelection();
			if (results.length > 0) {
				this.listener(results);
			}

			this.stop();
		}
	}

	activate() {
		this.input.on("pointerdown", this.onDown);
		this.input.on("pointerup", this.onUp);

		this.rect = this.game.add.rectangle(0, 0, 12, 12, 0x336688, 0.6);
		this.rect.setVisible(false);
		this.rect.setOrigin(0, 0);
	}

	deactivate() {
		this.input.removeListener("pointerdown", this.onDown);
		this.input.removeListener("pointerup", this.onUp);

		if (this.rect) {
			this.rect.destroy();
			this.rect = undefined;
		}
	}

	start() {
		this.selectionStart = this.game.getMousePosition().clone();
	}

	stop() {
		this.selectionStart = undefined;
		this.rect.setVisible(false);
	}

	getSelection(): Vec2[] {
		if (!this.selectionStart) {
			return [];
		}

		const [min, max] = MinMaxV(this.selectionStart,
				this.game.getMousePosition());

		const ret: Vec2[] = [];

		switch (this.selectType) {
		case ToolSelectType.Filled:
			for (let y = min.y; y <= max.y; y++) {
				for (let x = min.x; x <= max.x; x++) {
					ret.push(V(x, y));
				}
			}
			break;

		case ToolSelectType.Vertical:
			for (let y = min.y; y <= max.y; y++) {
				ret.push(V(this.selectionStart.x, y));
			}
			break;
		}

		return ret;
	}

	update(dtime: number): void {
		if (this.selectionStart) {
			this.rect.setVisible(true);

			const [min, max] = MinMaxV(this.selectionStart,
					this.game.getMousePosition());

			const pos = V(min.x, min.y);
			const size = V(max.x - min.x + 1, max.y - min.y + 1);
			if (this.selectType == ToolSelectType.Vertical) {
				pos.x = this.selectionStart.x;
				size.x = 1;
			}

			this.rect.setPosition(pos.x * 8, pos.y * 8);
			this.rect.setSize(size.x * 8, size.y * 8);
		}
	}
}
