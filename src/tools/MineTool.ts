import { Tool, ToolSpec, ToolUser } from "tools";
import { V } from "utils/phaser";
import { Layer } from "world/DefinitionManager";
import { WorkType } from "world/WorkManager";
import SelectHelper from "./SelectHelper";

export class MineTool implements Tool {
	private selectHelper: SelectHelper;

	constructor(private game: ToolUser, private input: Phaser.Input.InputPlugin,
			private spec: ToolSpec) {
		this.selectHelper = new SelectHelper(this.input, this.spec.selectType,
				this.game, (tiles) => {
			tiles.filter(pos => game.world.getDef(Layer.TERRAIN, pos)?.whenDug)
				.forEach(pos =>
					game.world.workManager.addWork({
						id: 0,
						type: WorkType.Mine,
						coords: V(0, 0),
						position: pos
					}));
		});
	}

	activate() {
		this.selectHelper.activate();
	}

	deactivate() {
		this.selectHelper.deactivate();
	}

	update(dtime: any): void {
		this.selectHelper.update(dtime);
	}
}
