import { Tool, ToolSpec, ToolUser } from "tools";
import SelectHelper from "./SelectHelper";

export class CancelTool implements Tool {
	private selectHelper: SelectHelper;

	constructor(private game: ToolUser, private input: Phaser.Input.InputPlugin,
			private spec: ToolSpec) {
		this.selectHelper = new SelectHelper(this.input, this.spec.selectType,
				this.game, (tiles) => {
			tiles.forEach(tile =>
					game.world.workManager.removeAllWorkAt(tile));
		});
	}

	activate() {
		this.selectHelper.activate();
	}

	deactivate() {
		this.selectHelper.deactivate();
	}

	update(dtime: any): void {
		this.selectHelper.update(dtime);
	}
}
