import { Vec2 } from "utils/phaser";
import World from "world/World";

export interface Tool {
	activate(): void;
	deactivate(): void;
	update(dtime: number): void;
}

export interface ToolUser {
	readonly world: World;

	getMousePosition(): Vec2;
	add: Phaser.GameObjects.GameObjectFactory;
}

export enum ToolType {
	Cancel,
	Mine,
	Build,
}

export enum ToolSelectType {
	Filled,   ///< Filled selection area
	Vertical, ///< Vertical line
}

export interface ToolSpec {
	type: ToolType;
	label: string;
	icon: string;
	selectType: ToolSelectType;
	info?: any;
}
