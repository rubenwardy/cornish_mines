import { Tool, ToolSpec, ToolUser } from "tools";
import { Layer, TileDef } from "world/DefinitionManager";
import { WorkType } from "world/WorkManager";
import SelectHelper from "./SelectHelper";

export class BuildTool implements Tool {
	private selectHelper: SelectHelper;
	private tileDef: TileDef;

	constructor(private game: ToolUser, private input: Phaser.Input.InputPlugin,
			private spec: ToolSpec) {
		this.tileDef = game.world.definitionManager.getDefById(spec.info.tile);
		console.assert(this.tileDef);

		this.selectHelper = new SelectHelper(this.input, this.spec.selectType,
				this.game, (tiles) => {
			tiles
				.filter(pos => game.world.get(Layer.TILE, pos) == 0)
				.forEach(pos =>
					game.world.workManager.addWork({
						id: 0,
						type: WorkType.Build,
						coords: this.tileDef.coords,
						position: pos,
						info: this.spec.info,
					}));
		});
	}

	activate() {
		this.selectHelper.activate();
	}

	deactivate() {
		this.selectHelper.deactivate();
	}

	update(dtime: any): void {
		this.selectHelper.update(dtime);
	}
}
