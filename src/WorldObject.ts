import World from "world/World";
import "phaser";
import DefinitionManager, { Layer } from "world/DefinitionManager";
import { V, Vec2 } from "utils/phaser";

export default class WorldObject extends Phaser.GameObjects.GameObject {
	private map: Phaser.Tilemaps.Tilemap;
	private layers: Phaser.Tilemaps.TilemapLayer[] = [];
	private blueprintLayer: Phaser.Tilemaps.TilemapLayer;

	private tiles: Phaser.Tilemaps.Tileset;
	private blueprintTiles: Phaser.Tilemaps.Tileset;

	constructor(scene: Phaser.Scene, private world: World,
			private definitionManager: DefinitionManager) {

		super(scene, "MapScene");

		this.world.onChange((pos, layer) => {
			const tmLayer = this.layers[layer];
			const id = this.world.get(layer, pos);
			if (id == 0) {
				tmLayer.removeTileAt(pos.x, pos.y);
			} else {
				tmLayer.putTileAt(this.makeTile(id), pos.x, pos.y);
			}
		});

		this.world.workManager.onChange(pos => {
			const worklist = this.world.workManager.getWorkListAt(pos);
			if (worklist.length == 0) {
				this.blueprintLayer.removeTileAt(pos.x, pos.y);
			} else {
				const work = worklist[0];
				const id = work.coords.x + work.coords.y * this.blueprintTiles.columns;
				this.blueprintLayer.putTileAt(id, pos.x, pos.y);
			}

		});

		this.recreate();
	}

	recreate() {
		this.map = this.scene.make.tilemap({
			width: this.world.width,
			height: this.world.height,
			tileWidth: 8,
			tileHeight: 8
		});

		this.tiles = this.map.addTilesetImage("Tileset", "tileset");
		this.blueprintTiles = this.map.addTilesetImage("Blueprint", "blueprint");
		this.createTileLayer(Layer.TERRAIN);
		this.createTileLayer(Layer.TILE);
		this.createWorkLayer();
	}

	private makeTile(id: number): number {
		const tileDef = this.definitionManager.getDefById(id);

		return  Math.floor(Math.random() * tileDef.choices) +
			tileDef.coords.x + tileDef.coords.y * this.tiles.columns;
	}

	private createTileLayer(layer: Layer) {
		const tmLayer = this.map.createBlankLayer(Layer[layer], this.tiles);
		this.layers.push(tmLayer);

		for (let y = 0; y < this.world.height; y++) {
			for (let x = 0; x < this.world.width; x++) {
				const id = this.world.get(layer, V(x, y));
				if (id == 0) {
					continue;
				}

				tmLayer.putTileAt(this.makeTile(id), x, y);
			}
		}
	}

	private createWorkLayer() {
		this.blueprintLayer = this.map.createBlankLayer("work", this.blueprintTiles);
		this.blueprintLayer.setAlpha(0.35);
	}

	getMousePosition(pointer: Phaser.Input.Pointer): Vec2 {
		const ret = this.map.worldToTileXY(pointer.worldX, pointer.worldY);

		return V(Math.floor(ret.x), Math.floor(ret.y));
	}
}
