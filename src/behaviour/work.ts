import { BTNode, BTState } from "behaviour";
import { Vec2 } from "utils/phaser";
import { Layer } from "world/DefinitionManager";
import WorkManager, { Work } from "world/WorkManager";
import World from "world/World";


export class BTWorkLock extends BTNode {
	constructor(private workManager: WorkManager,
			private work: Work,
			public children: BTNode[]) {
		super();
		console.assert(children.length == 1);
	}

	onStart() {
		this.workManager.lockPosition(this.work.position);
	}

	onFinish(state: BTState) {
		this.workManager.unlockPosition(this.work.position);
		if (state == BTState.Success) {
			this.workManager.removeWork(this.work);
		} else {
			this.workManager.notifyFailure(this.object.entity, this.work);
		}
	}

	onStep(...args: any[]): (BTState | undefined) {
		return this.children[0].run(...args);
	}
}


export class BTEntityLock extends BTNode {
	constructor(private workManager: WorkManager,
			private work: Work,
			public children: BTNode[]) {
		super();
		console.assert(children.length == 1);
	}

	get entityId() {
		return this.work.info.entity_id;
	}

	onStart() {
		this.workManager.lockEntity(this.entityId);
	}

	onFinish(state: BTState) {
		this.workManager.unlockEntity(this.entityId);
		if (state != BTState.Success) {
			this.workManager.notifyFailure(this.object.entity, this.work);
		}
	}

	onStep(...args: any[]): (BTState | undefined) {
		return this.children[0].run(...args);
	}
}


export class BTMine extends BTNode {
	private progress = 0;

	constructor(private target: Vec2) {
		super();
	}

	onStep(dtime: number, ...args: any[]): BTState {
		this.progress += dtime;
		if (this.progress > 5) {
			const world = this.object.entity.world as World;
			world.remove(Layer.TERRAIN, this.target);
			return BTState.Success;
		}

		return BTState.Running;
	}
}


export class BTBuild extends BTNode {
	private progress = 0;

	constructor(private target: Vec2, private layer: Layer, private tileId: number) {
		super();
	}

	onStep(dtime: number, ...args: any[]): BTState {
		this.progress += dtime;
		if (this.progress > 5) {
			const world = this.object.entity.world as World;
			world.set(this.layer, this.target, this.tileId);
			return BTState.Success;
		}

		return BTState.Running;
	}
}


export class CarryItem extends BTNode {
	constructor(private entityId: number, public children: BTNode[]) {
		super();
		console.assert(children.length == 1);
	}

	private get targetEntity() {
		const world = this.object.entity.world as World;
		return world.getEntityById(this.entityId);
	}

	onStart() {
		this.targetEntity.parent = this.object.entity;
	}

	onFinish(state: BTState) {
		if (this.targetEntity) {
			this.targetEntity.parent = undefined;
		}
	}

	onStep(...args: any[]): (BTState | undefined) {
		return this.children[0].run(...args);
	}
}
