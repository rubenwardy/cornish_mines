import { BTNode, BTState } from "behaviour";
import { getPathfinder } from "Pathfinder";
import { FloorV, V, Vec2 } from "utils/phaser";
import Entity from "world/Entity";


export abstract class FollowPath extends BTNode {
	pathIdx = 0;
	path: Vec2[];
	currentNodeTime = 0;
	timesFailed = 0;
	private failed = false;

	abstract calculatePath(): Vec2[];

	onStart() {
		this.failed = false;
	}

	onStep(dtime: number, ...args: any[]): (BTState | undefined) {
		if (this.failed) {
			return BTState.Failed;
		}

		console.assert(typeof dtime == "number");

		// Attempt to calculate path
		if (!this.path) {
			this.path = this.calculatePath();
			if (!this.path) {
				this.failed = true;
				return BTState.Failed;
			}
		}

		const entity = this.object.entity as Entity;
		const canClimb = entity.canClimb();
		const from = entity.position.clone();
		const next = this.path[this.pathIdx];
		if (!next) {
			return BTState.Success;
		}

		// Detect blockages by timeout, and recalculate
		this.currentNodeTime += dtime;
		if (this.currentNodeTime > 5) {
			this.timesFailed++;
			if (this.timesFailed >= 3) {
				this.failed = true;
				return BTState.Failed;
			}

			this.path = undefined;
			return BTState.Running;
		}

		const delta = next.clone().subtract(from);
		const needsJump = delta.y < -1/8;
		if (needsJump && !canClimb) {
			delta.y = 0;
		}

		if (canClimb && Math.abs(delta.x) > 0.1) {
			delta.y *= 0.9;
		}

		const distance = delta.length();
		const stepSize = (canClimb ? 1 : 3) * dtime;
		if (stepSize > distance) {
			this.pathIdx++;
			this.currentNodeTime = 0;
			return this.onStep(dtime, ...args);
		}

		if (!needsJump || canClimb) {
			if (distance * distance < 0.5 && this.pathIdx < this.path.length - 1) {
				this.pathIdx++;
				this.currentNodeTime = 0;
				return BTState.Running;
			}

			if ((!needsJump || canClimb) && distance < 0.2 && this.pathIdx == this.path.length - 1) {
				entity.moveTo(next);
				this.pathIdx = this.path.length;
				this.currentNodeTime = 0;
				return BTState.Success;
			}
		}

		const step = delta.clone().scale(stepSize / distance);
		if (needsJump && entity.stepUp(2 * step.x)) {
			return BTState.Running;;
		}


		entity.move(step);
		return BTState.Running;
	}
}


export class GoTo extends FollowPath {
	constructor(private to: Vec2) {
		super();
	}

	calculatePath(): Phaser.Math.Vector2[] {
		const entity = this.object.entity as Entity;

		const pathfinder = getPathfinder();
		const path = pathfinder.findPath(
				FloorV(entity.position.clone().add(V(0, -0.1))),
				FloorV(this.to));

		return path?.map(pos => pos.add(V(0.5, 0.9)));
	}
}

export class GoToNeighbour extends FollowPath {
	constructor(private to: Vec2) {
		super();
	}

	calculatePath(): Phaser.Math.Vector2[] {
		const entity = this.object.entity as Entity;

		const pathfinder = getPathfinder();
		const path = pathfinder.findPath(
				FloorV(entity.position.clone().add(V(0, -0.1))),
				FloorV(this.to), { acceptNeighbour: true });

		return path?.map(pos => pos.add(V(0.5, 0.9)));
	}
}
