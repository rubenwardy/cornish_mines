export enum BTState {
	Running,
	Failed,
	Success,
}


export class BehaviourTree {
	state: BTState;
	children: BTNode[];

	private seen: string[];

	constructor(node: BTNode, object: any) {
		this.children = [ node ];
		node.attach(this, object);
	}

	onEnter(node: BTNode) {
		this.seen.push(node.constructor.name);
	}

	onExit(_node: BTNode, _state: BTState) {}

	run(...args: any[]) {
		this.seen = [];
		this.state = BTState.Running;

		const state = this.children[0].run(...args);
		if (state) {
			this.state = state;
		}

		// console.log(BTState[this.state], this.seen.join(", "));

		return this.state;
	}

	dump() {
		return `${BTState[this.state]} ${this.seen.join(", ")}`;
	}
}


export abstract class BTNode {
	protected tree: BehaviourTree;
	protected object: any;
	protected state: BTState;
	protected started: boolean = false;
	readonly children: BTNode[] = [];

	attach(tree: BehaviourTree, object: any) {
		this.tree = tree;
		this.object = object;
		this.children.forEach((child) =>
				child.attach(tree, object));
	}

	getState() {
		return this.state;
	}

	onStart(): void {}
	onFinish(_state: BTState): void {}

	abstract onStep(...args: any[]): (BTState | undefined);

	run(...args: any[]) {
		this.state = BTState.Running;

		this.tree.onEnter(this);

		if (!this.started) {
			this.onStart();
			this.started = true;
		}

		const state = this.onStep(...args);
		if (state) {
			this.state = state;
		} else {
			this.state = this.getState();
		}

		this.tree.onExit(this, state);

		if (this.state != BTState.Running) {
			/* if (this.state != BTState.Success) {
				console.log("Node failure at " + this.constructor.name);
			} */

			this.started = false;
			this.onFinish(this.state);
		}

		return this.state;
	}
}


export class Sequence extends BTNode {
	index = 0;

	constructor(public children: BTNode[]) {
		super();
	}

	onStep(...args: any[]): (BTState | undefined) {
		while (this.index < this.children.length) {
			const child = this.children[this.index];
			const state = child.run(...args);
			if (state == BTState.Running || state == BTState.Failed) {
				return state;
			}

			this.index++;
		}

		this.index = 0;
		return BTState.Success;
	}
}


export class Selector extends BTNode {
	index = 0;

	constructor(public children: BTNode[]) {
		super();
	}

	onStep(...args: any[]): (BTState | undefined) {
		while (this.index < this.children.length) {
			const child = this.children[this.index];
			const state = child.run(...args);
			if (state == BTState.Running) {
				return BTState.Running;
			} else if (state == BTState.Success) {
				this.index = 0;
				return state;
			}

			this.index++;
		}
		return BTState.Failed;
	}
}


export function makeNode(func: (node: BTNode, ...args: any[]) => void, isDeco: boolean) {
	return class XX extends BTNode {
		constructor(public args: any,
				public children: BTNode[]) {
			super();

			if (isDeco) {
				console.assert(children.length == 1);
			}
		}

		onStep(...args: any[]): (BTState | undefined) {
			return func.call(this, ...args);
		}
	}
}


export function makeDecorator(func: (node: BTNode, ...args: any[]) => void) {
	return makeNode(func, true);
}


export const Failure = makeDecorator((node: BTNode, ...args: any[]) => {
	if (node.children[0].run(...args) != BTState.Running) {
		return BTState.Failed;
	}
});


export const Success = makeDecorator((node: BTNode, ...args: any[]) => {
	if (node.children[0].run(...args) != BTState.Running) {
		return BTState.Failed;
	}
});
