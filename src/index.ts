import { GameScene } from "GameScene";
import { LoadingScene } from "LoadingScene";
import "phaser";

import "./scss/main.scss";

const config: Phaser.Types.Core.GameConfig = {
	title: "Mines",
	type: Phaser.WEBGL,
	width: 800,
	height: 640,
	fps: { target: 60 },
	parent: "game",
	disableContextMenu: true,
	scene: [LoadingScene, GameScene],
	scale: {
		mode: Phaser.Scale.RESIZE
	},
	render: {
		pixelArt: true
	}
};

window.onload = () => {
	new Phaser.Game(config);
};
