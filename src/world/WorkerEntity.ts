import { BehaviourTree, BTNode, BTState, Sequence } from "behaviour";
import { GoTo, GoToNeighbour } from "behaviour/goto";
import { BTBuild, BTEntityLock, BTMine, BTWorkLock, CarryItem } from "behaviour/work";
import { Vec2 } from "utils/phaser";
import Entity, { EntityType } from "./Entity";
import { WorkType } from "./WorkManager";
import World from "./World";

export default class WorkerEntity extends Entity {
	private bt: BehaviourTree;
	private lastFindWorkFailure = 1000;

	constructor(world: World, texture: string, position: Vec2) {
		super(EntityType.Worker, world, texture, position);

		this.gameObject.setDepth(10);
	}

	update(dtime: number) {
		this.lastFindWorkFailure += dtime;

		if (this.bt && this.bt.run(dtime) != BTState.Running) {
			this.bt = null;
		}

		if (!this.bt) {
			this.findWork();

			if (this.bt && this.bt.run(0) != BTState.Running) {
				this.bt = null;
			}
		}

		super.update(dtime);
	}

	setBehaviourTree(root: BTNode) {
		this.bt = new BehaviourTree(root, { entity: this });
	}

	findWork() {
		if (this.lastFindWorkFailure < 3) {
			return;
		}

		const work = this.world.workManager.findWorkNear(this, this.position);
		if (!work) {
			this.lastFindWorkFailure = 0;
			return;
		}


		switch (work.type) {
		case WorkType.Mine:
			this.setBehaviourTree(
				new BTWorkLock(this.world.workManager, work, [
					new Sequence([
						new GoToNeighbour(work.position.clone()),
						new BTMine(work.position.clone()),
					])
				]));
			break;

		case WorkType.Build:
			if (this.world.collidesAt(work.position)) {
				this.world.workManager.notifyFailure(this, work);
				return;
			}

			this.setBehaviourTree(
				new BTWorkLock(this.world.workManager, work, [
					new Sequence([
						new GoToNeighbour(work.position.clone()),
						new BTBuild(work.position.clone(), work.info.layer, work.info.tile),
					])
				]));
			break;

		case WorkType.MoveItem:
			this.setBehaviourTree(
				new BTEntityLock(this.world.workManager, work, [
					new Sequence([
						new GoTo(work.position.clone()),
						new CarryItem(work.info.entity_id, [
							new GoTo(work.info.target.clone()),
						]),
					])
				]));
			break;

		default:
			console.error(`Unimplemented WorkType ${WorkType[work.type] ?? work.type}`);
			this.world.workManager.notifyFailure(this, work);
			break;
		}
	}

	dump() {
		if (this.lastFindWorkFailure < 3) {
			console.assert(this.bt == undefined);
			return `${this.id} | findWork lockout: ${(3 - this.lastFindWorkFailure).toFixed(1)}s`;
		}

		return `${this.id} | ${this.bt ? this.bt.dump() : "-"}`;
	}
}
