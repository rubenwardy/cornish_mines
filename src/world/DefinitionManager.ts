import "phaser";
import { V, Vec2 } from "utils/phaser";

export enum Layer {
	TERRAIN,
	TILE,
}

export interface TileDef {
	id?: number;
	name: string;
	coords: Vec2;
	choices: number;
	collides: boolean;
	whenDug?: string;
	dropCoords?: Vec2;
	climbable: boolean;
}

export interface Building {
	name: string;
	position: Vec2;

	startId: number;
	size: Vec2;
}

export default class DefinitionManager {
	private tileDefs: TileDef[] = []
	private tileDefByName = new Map<string, TileDef>();
	private buildingDefByName = new Map<string, Building>();

	getDefById(id: number): TileDef {
		const def = this.tileDefs[id - 1];
		if (!def) {
			console.error(`Unable to find def with id=${id}`);
		}
		return def;
	}

	getDefByName(name: string): TileDef {
		const def = this.tileDefByName.get(name);
		if (!def) {
			console.error(`Unable to find def with id=${name}`);
		}
		return def;
	}

	getIdByName(name: string): (number | undefined) {
		if (name == "air") {
			return 0;
		}

		return this.getDefByName(name)?.id ?? undefined;
	}

	getBuildingByName(name: string): Building {
		return this.buildingDefByName.get(name);
	}

	register(def: TileDef): number {
		this.tileDefs.push(def);
		this.tileDefByName.set(def.name, def);

		def.id = this.tileDefs.length;
		return this.tileDefs.length;
	}

	registerAll(defs: TileDef[]): number[] {
		return defs.map((def) => this.register(def));
	}

	registerBuilding(name: string, start: Vec2, size: Vec2) {
		this.buildingDefByName.set(name, {
			name: name,
			position: V(),
			startId: this.tileDefs.length + 1,
			size: size.clone(),
		});

		for (let x = 0; x < size.x; x++) {
			for (let y = 0; y < size.y; y++) {
				this.register({
					name: `${name}_${x}_${y}`,
					coords: start.clone().add(V(x, y)),
					choices: 1,
					collides: false,
					climbable: false,
				});
			}
		}
	}
}



export enum TileIds {
	AIR = 0,
	GRASS,
	DIRT,
	STONE,
	GRAVEL,
	TIN_ORE,
	DUG_GRASS,
	DUG_DIRT,
	DUG_STONE,
	DUG_GRAVEL,

	LADDER,

	ENGINE_HOUSE_0_0,
	ENGINE_HOUSE_0_1,
	ENGINE_HOUSE_0_2,
	ENGINE_HOUSE_0_3,
	ENGINE_HOUSE_0_4,
	ENGINE_HOUSE_0_5,
	ENGINE_HOUSE_1_0,
	ENGINE_HOUSE_1_1,
	ENGINE_HOUSE_1_2,
	ENGINE_HOUSE_1_3,
	ENGINE_HOUSE_1_4,
	ENGINE_HOUSE_1_5,
	ENGINE_HOUSE_2_0,
	ENGINE_HOUSE_2_1,
	ENGINE_HOUSE_2_2,
	ENGINE_HOUSE_2_3,
	ENGINE_HOUSE_2_4,
	ENGINE_HOUSE_2_5,
};

export function defineDefaults(defman: DefinitionManager) {
	defman.registerAll([
		{
			name: "grass",
			coords: V(0, 0),
			choices: 2,
			whenDug: "dug_grass",
			collides: true,
			climbable: false,
		},
		{
			name: "dirt",
			coords: V(0, 1),
			choices: 2,
			whenDug: "dug_dirt",
			collides: true,
			climbable: false,
		},
		{
			name: "stone",
			coords: V(0, 3),
			choices: 2,
			whenDug: "dug_stone",
			dropCoords: V(0, 0),
			collides: true,
			climbable: false,
		},
		{
			name: "gravel",
			coords: V(0, 4),
			choices: 2,
			whenDug: "dug_gravel",
			collides: true,
			climbable: false,
		},
		{
			name: "tin_ore",
			coords: V(0, 5),
			choices: 2,
			whenDug: "dug_stone",
			dropCoords: V(0, 1),
			collides: true,
			climbable: false,
		},

		{
			name: "dug_grass",
			coords: V(2, 0),
			choices: 1,
			collides: false,
			climbable: false,
		},
		{
			name: "dug_dirt",
			coords: V(2, 1),
			choices: 1,
			collides: false,
			climbable: false,
		},
		{
			name: "dug_stone",
			coords: V(2, 3),
			choices: 1,
			collides: false,
			climbable: false,
		},
		{
			name: "dug_gravel",
			coords: V(2, 4),
			choices: 1,
			collides: false,
			climbable: false,
		},

		{
			name: "ladder",
			coords: V(3, 0),
			choices: 1,
			collides: false,
			climbable: true,
		},
	]);

	defman.registerBuilding("engine_house", V(9, 0), V(3, 6));
}
