import { FloorV, HashV, V, Vec2 } from "utils/phaser";
import Entity, { EntityType } from "./Entity";
import ItemEntity from "./ItemEntity";
import World from "./World";


export enum WorkType {
	Mine,
	Build,
	MoveItem,
}


export interface Work {
	id: number;
	type: WorkType;
	position: Vec2;
	coords: Vec2;
	info?: any;
}


type Listener = (pos: Vec2) => void;

export default class WorkManager {
	private id_counter = 0;
	private work = new Map<number, Work[]>()
	private workById = new Map<number, Work>();
	private locks = new Set<number>();
	private entityLocks = new Set<number>();
	private failureTimeouts = new Map<number, number>();
	private listeners: Listener[] = [];

	constructor(private world: World) {}

	getWorkListAt(position: Vec2): Work[] {
		const hash = HashV(FloorV(position));

		let list = this.work.get(hash);
		if (!list) {
			list = [];
			this.work.set(hash, list);
		}

		return list;
	}

	private notifyChange(pos: Vec2) {
		this.listeners.forEach(listener => listener(pos.clone()));
	}

	addWork(work: Work): Work {
		this.id_counter++;
		work.id = this.id_counter;

		const worklist = this.getWorkListAt(work.position);
		if (worklist.some(other => other.type == work.type)) {
			return null;
		}

		worklist.push(work);
		this.workById.set(work.id, work);

		this.notifyChange(work.position);

		return work;
	}

	getWorkById(id: number): Work {
		return this.workById.get(id);
	}

	removeWork(work: (Work | number)) {
		if (typeof work == "number") {
			work = this.getWorkById(work);
		}
		if (!work || work.id == -1) {
			return;
		}

		this.workById.delete(work.id);

		const worklist = this.getWorkListAt(work.position);
		const idx = worklist.indexOf(work);
		worklist.splice(idx, 1);

		// Clear failures for tile
		worklist.forEach(work2 => this.failureTimeouts.delete(work2.id));

		this.notifyChange(work.position);
	}

	removeAllWorkAt(pos: Vec2) {
		const worklist = this.getWorkListAt(pos);
		worklist.splice(0, worklist.length);

		this.notifyChange(pos);
	}

	onChange(listener: Listener) {
		this.listeners.push(listener);
	}

	isPositionLocked(pos: Vec2): boolean {
		return this.locks.has(HashV(FloorV(pos)));
	}

	lockPosition(pos: Vec2) {
		// console.log(`Locked position (${pos.x}, ${pos.y})`);
		this.locks.add(HashV(FloorV(pos)));
	}

	unlockPosition(pos: Vec2) {
		// console.log(`Unlocked position (${pos.x}, ${pos.y})`);
		this.locks.delete(HashV(FloorV(pos)));
	}

	lockEntity(id: number) {
		// console.log(`Locked entity ${id}`);
		this.entityLocks.add(id);
	}

	unlockEntity(id: number) {
		// console.log(`Locked entity ${id}`);
		this.entityLocks.delete(id);
	}

	hasFailure(entity: Entity, workId: number) {
		const id = entity.id * 1000000000 + workId;
		return this.failureTimeouts.has(id);
	}

	notifyFailure(entity: Entity, work: Work) {
		const id = entity.id * 1000000000 + work.id;
		this.failureTimeouts.set(id, 10);
	}

	findWorkNear(worker: Entity, pos: Vec2) {
		let bestWork: Work = undefined;
		let bestDistanceSq = Number.MAX_SAFE_INTEGER;

		for (let itemEntity of this.world.getEntitiesOfType<ItemEntity>(EntityType.Item)) {
			const workId = itemEntity.id + 100000;
			if (itemEntity.isInStockpile() ||
					this.entityLocks.has(itemEntity.id) ||
					this.hasFailure(worker, workId)) {
				continue;
			}

			const distanceSq = itemEntity.position.distanceSq(pos) + 2*2;
			if (distanceSq < bestDistanceSq) {
				bestWork = {
					id: workId,
					type: WorkType.MoveItem,
					position: FloorV(itemEntity.position.clone().add(V(0, -0.1))),
					coords: V(0, 0),
					info: {
						entity_id: itemEntity.id,
						target: this.world.getPoint("stockpile").clone()
								.add(V(Math.random() * 2, 0)),
					},
				};
				bestDistanceSq = distanceSq;
			}
		}

		for (let [hash, worklist] of this.work) {
			if (this.locks.has(hash) || worklist.length == 0) {
				continue;
			}

			const distanceSq = worklist[0].position.distanceSq(pos);
			if (distanceSq >= bestDistanceSq) {
				continue;
			}

			for (let work of worklist) {
				if (!this.hasFailure(worker, work.id)) {
					bestWork = work;
					bestDistanceSq = distanceSq;
				}
			}
		}

		return bestWork;
	}

	update(dtime: number) {
		for (let key of this.failureTimeouts.keys()) {
			const next = this.failureTimeouts.get(key) - dtime;
			if (next > 0) {
				this.failureTimeouts.set(key, next);
			} else {
				this.failureTimeouts.delete(key);
			}
		}
	}
}
