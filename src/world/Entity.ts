import { FloorV, RoundV, V, Vec2 } from "utils/phaser";
import World from "./World";

export enum EntityType {
	Worker,
	Item,
}

export default abstract class Entity {
	protected gameObject: Phaser.GameObjects.Image;
	id: number = -1;
	parent: Entity = undefined;

	constructor(
			readonly type: EntityType,
			protected world: World,
			readonly texture: string,
			public position: Vec2) {
		this.gameObject = this.world.add.image(this.position.x, this.position.y, this.texture);
		this.gameObject.setOrigin(1 / 3, 1);
	}

	destroy() {
		this.gameObject.destroy();
	}

	moveTo(position: Vec2) {
		if (this.parent) {
			return;
		}

		if (this.world.collidesAt(FloorV(position.clone().subtract(V(0, 0.01))))) {
			return;
		}

		this.position = position;
	}

	move(offset: Vec2) {
		if (this.parent) {
			return;
		}

		this.moveTo(this.position.clone().add(V(offset.x, 0)));
		this.moveTo(this.position.clone().add(V(0, offset.y)));
	}

	/**
	 * Attempts to step up
	 *
	 * @param xStep The horizontal distance to step across
	 * @returns success bool
	 */
	stepUp(xStep: number): boolean {
		if (this.parent) {
			return false;
		}

		const up = this.position.clone().add(V(0, -1.1));
		const target = this.position.clone().add(V(xStep, -1.1));
		if (Math.floor(target.x) == Math.floor(this.position.x) ||
				this.world.collidesAt(up) || !this.world.canStandAt(target)) {
			return false;
		}

		this.moveTo(target);
		return true;
	}

	canClimb() {
		if (this.parent) {
			return false;
		}

		return this.world.climbableAt(this.position);
	}

	update(dtime: number) {
		if (this.parent) {
			this.position = this.parent.position.clone();
		}

		if (this.world.collidesAt(this.position.clone().add(V(0, -0.1)))) {
			console.error(`${EntityType[this.type]} ${this.id} is stuck at offset position!`);
		}

		if (!this.parent && !this.canClimb()) {
			this.move(V(0, dtime * 3));
		}

		const pxPosition = RoundV(this.position.clone().scale(8));
		this.gameObject.setPosition(pxPosition.x, pxPosition.y);
	}
}
