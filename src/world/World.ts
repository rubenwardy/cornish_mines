import "phaser";
import { FloorV, V, Vec2 } from "utils/phaser";
import DefinitionManager, { Building, Layer, TileDef } from "./DefinitionManager";
import Entity, { EntityType } from "./Entity";
import ItemEntity from "./ItemEntity";
import WorkManager from "./WorkManager";

type Listener = (pos: Vec2, layer: Layer) => void;

export default class World {
	private terrain: number[];
	private tiles: number[];
	private entities: Entity[] = [];
	readonly buildings: Building[] = [];
	readonly workManager: WorkManager;
	private points = new Map<string, Vec2>();

	private entity_id_counter = 0;
	private listeners: Listener[] = [];

	constructor(
			readonly add: Phaser.GameObjects.GameObjectFactory,
			readonly definitionManager: DefinitionManager, readonly width: number, readonly height: number) {
		this.terrain = new Array(width * height).fill(0);
		this.tiles = new Array(width * height).fill(0);
		this.workManager = new WorkManager(this);
	}

	get(layer: Layer, pos: Vec2): number {
		switch (layer) {
		case Layer.TERRAIN:
			return this.getTerrain(pos);
		case Layer.TILE:
			return this.getTile(pos);
		default:
			throw Error(`Unknown layer: ${layer}`);
		}
	}

	set(layer: Layer, pos: Vec2, id: number) {
		switch (layer) {
		case Layer.TERRAIN:
			this.setTerrain(pos, id);
			break;
		case Layer.TILE:
			this.setTile(pos, id);
			break;
		default:
			throw Error(`Unknown layer: ${layer}`);
		}
	}

	remove(layer: Layer, pos: Vec2) {
		switch (layer) {
		case Layer.TERRAIN:
			const def = this.getDef(Layer.TERRAIN, pos);
			if (def && def.whenDug) {
				this.setTerrain(pos,
					this.definitionManager.getIdByName(def.whenDug!));
			}
			if (def && def.dropCoords) {
				this.dropItem(pos, def.name);
			}
			break;
		case Layer.TILE:
			this.setTile(pos, 0);
			break;
		default:
			throw Error(`Unknown layer: ${layer}`);
		}
	}

	getTerrain(pos: Vec2) {
		pos = FloorV(pos.clone());

		console.assert(pos.x < this.width);
		console.assert(pos.y < this.height);
		return this.terrain[pos.x + pos.y * this.width];
	}

	setTerrain(pos: Vec2, id: number) {
		pos = FloorV(pos.clone());

		console.assert(pos.x < this.width);
		console.assert(pos.y < this.height);
		this.terrain[pos.x + pos.y * this.width] = id;

		this.listeners.forEach(listener => listener(pos.clone(), Layer.TERRAIN));
	}

	getTile(pos: Vec2) {
		pos = FloorV(pos.clone());

		console.assert(pos.x < this.width);
		console.assert(pos.y < this.height);
		return this.tiles[pos.x + pos.y * this.width];
	}

	setTile(pos: Vec2, id: number) {
		pos = FloorV(pos.clone());

		console.assert(pos.x < this.width);
		console.assert(pos.y < this.height);
		this.tiles[pos.x + pos.y * this.width] = id;

		this.listeners.forEach(listener => listener(pos.clone(), Layer.TILE));
	}

	getDef(layer: Layer, pos: Vec2): (TileDef | undefined) {
		const id = this.get(layer, pos);
		if (id == 0) {
			return undefined;
		}

		return this.definitionManager.getDefById(id);
	}

	setRect(layer: Layer, startX: number, startY: number, startId: number, size: Vec2) {
		let id = startId;
		for (let x = startX; x < startX + size.x; x++) {
			for (let y = startY; y < startY + size.y; y++) {
				this.set(layer, V(x, y), id);
				id++;
			}
		}
	}

	placeBuilding(building: (Building | string), x: number, y: number) {
		if (typeof building == "string") {
			building = this.definitionManager.getBuildingByName(building);
		}

		this.setRect(Layer.TILE, x, y, building.startId, building.size);
		this.buildings.push({ ...building, position: V(x, y) });
	}

	onChange(func: Listener) {
		this.listeners.push(func);
	}

	update(dtime: number) {
		this.workManager.update(dtime);
		this.entities.forEach(entity => entity.update(dtime));
	}

	getHeightAt(layer: Layer, x: number) {
		let y = 0;
		while (this.get(layer, V(x, y)) == 0) {
			y++;
		}
		return y;
	}

	collidesAt(pos: Vec2): boolean {
		if (pos.x < 0 || pos.x >= this.width || pos.y < 0 || pos.y >= this.height) {
			return true;
		}

		const terrainDef = this.getDef(Layer.TERRAIN, pos);
		return terrainDef && terrainDef.collides;
	}

	canStandAt(pos: Vec2): boolean {
		if (pos.x < 0 || pos.x >= this.width || pos.y < 0 || pos.y >= this.height) {
			return false;
		}

		const below = pos.clone().add(V(0, 1));
		return this.climbableAt(pos) ||
			(!this.collidesAt(pos) &&
				(this.collidesAt(below) || this.climbableAt(below)));
	}

	climbableAt(pos: Vec2): boolean {
		if (pos.x < 0 || pos.x >= this.width || pos.y < 0 || pos.y >= this.height) {
			return false;
		}

		const terrainDef = this.getDef(Layer.TILE, pos);
		return terrainDef && terrainDef.climbable;
	}

	addEntity(entity: Entity) {
		this.entity_id_counter++;
		entity.id = this.entity_id_counter;
		this.entities.push(entity);
	}

	getEntityById(id: number) {
		return this.entities.find(entity => entity.id == id);
	}

	removeEntity(entity: (Entity | number)) {
		if (typeof entity == "number") {
			entity = this.getEntityById(entity);
		}

		entity.destroy();
		const idx = this.entities.indexOf(entity);
		this.entities.splice(idx, 1);
	}

	dropItem(pos: Vec2, item: string) {
		pos = FloorV(pos).add(V(Math.random() * 0.4 + 0.3, 0));
		this.addEntity(new ItemEntity(this, pos.clone(), item));
	}

	getEntitiesOfType<T extends Entity>(type: EntityType): T[] {
		return this.entities.filter(entity => entity.type == type) as T[];
	}

	getPoint(name: string): Vec2 {
		return this.points.get(name);
	}

	setPoint(name: string, v: Vec2) {
		console.log(`${name} = (${v.x}, ${v.y})`);
		this.points.set(name, v);
	}
}
