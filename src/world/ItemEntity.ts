import { Vec2 } from "utils/phaser";
import Entity, { EntityType } from "./Entity";
import World from "./World";

export default class ItemEntity extends Entity {
	private _item: string;

	constructor(world: World, position: Vec2, item: string) {
		super(EntityType.Item, world, "items", position);
		this.gameObject.setDepth(5);
		this.item = item;
	}

	get item() {
		return this._item;
	}

	set item(item: string) {
		this._item = item;

		const def = this.world.definitionManager.getDefByName(item);
		this.gameObject.setFrame(def.dropCoords.x + def.dropCoords.y * 1);
	}

	isInStockpile() {
		const stockpilePos = this.world.getPoint("stockpile");
		return (Math.abs(this.position.y - stockpilePos.y) <= 1.1 &&
				this.position.x >= stockpilePos.x - 0.1 &&
				this.position.x < stockpilePos.x + 2.1);
	}
}
