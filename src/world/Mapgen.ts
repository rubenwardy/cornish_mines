import World from "./World";
import noise from "../lib/perlin";
import { Layer, TileIds } from "./DefinitionManager";
import { V } from "utils/phaser";

export default function generateMap(world: World) {
	noise.seed(Math.random());

	const heights = Array.apply(null, Array(world.width)).map((_, x) =>
		20 + Math.floor(noise.simplex2(x / 50, 0) * 5));

	function isFlat(x: number, w: number) {
		const height = heights[x];
		for (let i = 1; i < w; i++) {
			if (heights[x + i] != height) {
				return false;
			}
		}

		return true;
	}

	for (let x = 0; x < world.width; x++) {
		const height = heights[x];

		world.setTerrain(V(x, height), 1);
		for (let y = height + 1; y < world.height; y++) {
			if (y < height + 5) {
				world.setTerrain(V(x, y), TileIds.DIRT);
			} else {
				world.setTerrain(V(x, y), TileIds.STONE);
			}
		}
	}

	for (let y = 10; y < world.height; y++) {
		for (let x = 0; x < world.width; x++) {
			const valueGravel = noise.simplex2(x / 30, y / 30);
			if (valueGravel > 0.7 && world.getTerrain(V(x, y)) == TileIds.STONE) {
				world.setTerrain(V(x, y), TileIds.GRAVEL);
				continue;
			}

			const valueTin = noise.simplex2(x / 20 + 300, y / 10) - Math.max(0, (heights[x] + 20) - y) / 10;
			if (valueTin > 0.6 && world.getTerrain(V(x, y)) == TileIds.STONE) {
				world.setTerrain(V(x, y), TileIds.TIN_ORE);
				continue;
			}
		}
	}

	for (let i = Math.floor(Math.random() * 10); i < world.width; i++) {
		const x = Math.floor(world.width / 2 + Math.floor(i / 2) * (i % 2 == 0 ? 1 : -1));
		const heightL = heights[x];
		if (!isFlat(x - 1, 5)) {
			continue;
		}

		world.placeBuilding("engine_house", x, heightL - 6);
		world.setPoint("engine_house", V(x, heightL - 6));

		let placedStockpile = false;
		for (let i = 0; i < 5; i++) {
			const spx = x + 3 + i;
			if (isFlat(spx, 2)) {
				world.setPoint("stockpile", V(spx, heights[spx] - 1));
				placedStockpile = true;
				break;
			}
		}

		if (!placedStockpile) {
			for (let i = 0; i < 5; i++) {
				const spx = x - i - 2;
				if (isFlat(spx, 2)) {
					world.setPoint("stockpile", V(spx, heights[spx] - 1));
					placedStockpile = true;
					break;
				}
			}
		}

		if (!placedStockpile) {
			const spx = x + 3;
			world.setPoint("stockpile", V(spx, heights[spx] - 1));
		}

		for (let y = heightL; y < heightL + 10; y++) {
			const def = world.getDef(Layer.TERRAIN, V(x + 1, y));
			if (!def || !def.whenDug) {
				return;
			}

			const whenDugId = world.definitionManager.getIdByName(def.whenDug);
			if (whenDugId !== undefined) {
				world.setTerrain(V(x + 1, y), whenDugId);
				world.setTile(V(x + 1, y), TileIds.LADDER);
			}
		}

		break;
	}
}
