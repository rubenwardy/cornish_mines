const path = require("path");
const CopyPlugin = require("copy-webpack-plugin");
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const HtmlWebpackPlugin = require("html-webpack-plugin");


const dest = path.resolve(__dirname, "dist");

module.exports = {
	entry: "./src/index.ts",
	module: {
		rules: [
			{
				test: /\.tsx?$/,
				use: [
					{
						loader: "ts-loader",
						options: {
							configFile: "tsconfig.json",
						}
					}
				],
				exclude: /node_modules/
			},
			{
				test: /\.handlebars$/,
				loader: "handlebars-loader"
			},
			{
				test: /\.s[ac]ss$/i,
				use: [
					MiniCssExtractPlugin.loader,
					"css-loader?url=false",
					"sass-loader"
				],
			},
			{
				test: /\.glsl$/i,
				use: "raw-loader"
			},
		]
	},
	plugins: [
		new CopyPlugin({
			patterns: [ { from: "src/public", to: dest }, ]
		}),
		new MiniCssExtractPlugin({
			// Options similar to the same options in webpackOptions.output
			// both options are optional
			filename: '[name].css',
			chunkFilename: '[id].css',
		}),
		new HtmlWebpackPlugin({
			title: "Mines",
			template: "src/templates/index.ejs",
		}),
	],
	resolve: {
		extensions: [ ".ts", ".tsx", ".js" ],
		modules: [
			path.resolve(__dirname, "src"),
			"node_modules"
		],

		alias: {
			"react": "preact/compat",
			"react-dom/test-utils": "preact/test-utils",
			"react-dom": "preact/compat",
		},
	},
	output: {
		filename: "app.js",
		path: dest
	},
	mode: "development"
};
